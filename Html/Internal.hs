-- module Html.Internal.hs

module Html.Internal where

-- *** Types 

newtype Html = Html String 
newtype Structure = Structure String 

type Title = String 
type Tag   = String

-- *** eDSL
-- body_ :: String -> String 
-- body_ = el "body"
-- 
-- head_ :: String -> String 
-- head_ = el "head"

-- Title 
title_ :: String -> Structure
title_ = Structure . el "title" . escape

-- Paragraph
p_ :: String -> Structure 
p_ = Structure . el "p" . escape

-- Header 1
h1_ :: String -> Structure
h1_ = Structure . el "h1" . escape

-- Unordered List
ul_ :: [Structure] -> Structure 
ul_ = Structure . el "ul" . concatMap liString
      where 
        liString = el "li" . structure2String  -- liString :: Structure -> String

-- Ordered List
ol_ :: [Structure] -> Structure 
ol_ = Structure . el "ol" . concatMap liString
      where 
        liString = el "li" . structure2String  -- liString :: Structure -> String

-- Code block
code_ :: String -> Structure
code_ = Structure . el "pre"

-- ***** eDSL functions ...

el :: Tag -> String -> String 
el tag content = "<" <> tag <> ">" <> content <> "</" <> tag <> ">"

escape :: String -> String
escape =
  let
    escapeChar c =
      case c of
        '<' -> "&lt;"
        '>' -> "&gt;"
        '&' -> "&amp;"
        '"' -> "&quot;"
        '\'' -> "&#39;"
        _ -> [c]
  in
    concat . map escapeChar

html_ :: Title -> Structure -> Html
html_ title struct = 
    Html (
      el "html"
        (el "head" (structure2String ts)
          <> el "body" (structure2String struct)
        )
      )
    where
      ts = title_ title

append_ :: Structure -> Structure -> Structure
append_ (Structure a) (Structure b)  = Structure (a <> b)

structure2String :: Structure -> String 
structure2String s = 
  case s of 
    Structure str -> str 

render :: Html -> String 
render h = 
  case h of 
    Html s -> s