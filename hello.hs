import Html

makeHtml :: String -> String -> String 
makeHtml title content = 
                    render $ 
                      html_ title (
                        append_ (h1_ title) 
                            (append_ (p_ content)
                                     (ul_ [ 
                                        p_ "item 1"
                                      , p_ "item 2"
                                      , p_ "item 3"
                                      ] )))


main = putStrLn $ makeHtml "Title this is > than other" "Content & other things"
